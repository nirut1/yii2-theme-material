<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace nirut1\theme\material;

use yii\web\AssetBundle;
class MaterialAsset extends AssetBundle{
    public $sourcePath='@vender/nirut1/yii2-theme-material/assets';
    public $baseUrl = '@web';
    
    public $css=[
        'css/bootstrap-material-design.min.css',
        'css/ripples.min.css',
    ];
    public $js=[
        'js/material.min.js',
        'js/ripples.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',     
    ];
    public function init(){
        parent::init();
    }
}