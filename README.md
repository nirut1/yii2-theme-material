Yii2 Material Desing and Bootstrap Theme
============================
Theme for Yii2 Web Application


Installation
-------------

```
php composer.phar require --perfer-dist nirut1/yii2-theme-material "*"
```

or add

```
"nirut1/yii2-theme-material": "*"
```

to the require section of your composer.json file.

Usage
------

Open your layout view/layout/main.php and add

```
use \nirut1\theme\material;

material\MaterialAsset::register($this);
```